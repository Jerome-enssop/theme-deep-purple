<?php get_header(); ?>

<div class="container mt-4">
    <div class="jumbotron">
        <h1 class="text-center">Gallerie<h1>
    </div>

    <div class="row">

    <?php
        if(have_posts()){
            while(have_posts()){
                the_post();
                ?>
                        <div class="col-md-3 col-6 mb-4" id="pictureGallerie"><img src="<?php the_post_thumbnail_url(); ?>" class="d-block col-12" alt=""> 
                        </div>
                    
                <?php
            }
        }
    ?>

    </div>

    <?php get_footer(); ?>
