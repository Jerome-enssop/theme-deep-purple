<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <?php wp_head(); ?>
</head>
<body>

<header>
    <nav>
        <a href="#">Deep Purple</a>
        <div>
            <?php wp_nav_menu( [
                "theme_location" => "nav_menu"
            ] ) ?>
        </div>
    </nav>
</header>