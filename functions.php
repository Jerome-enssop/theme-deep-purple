<?php

// Ajouter la prise en charge des images mises en avant
add_theme_support( 'post-thumbnails' );

if(!function_exists('depp_purple_post_type')){
    function deep_purple_post_type(){


       /**
        * Custom Post Type : membre
        */

       $labels = array(
       "name"                  => __( "Membre", "enssop" ),
       "singular_name"         => __( "Membre", "enssop" ),
       "menu_name"             => __( "Membre", "enssop" ),
       "all_items"             => __( "Tous les membres", "enssop" ),
       "add_new"               => __( "Ajouter un membre", "enssop" ),
       "add_new_item"          => __( "Ajouter un nouveau membre", "enssop" ),
       "edit_item"             => __( "Ajouter le membre", "enssop" ),
       "new_item"              => __( "Nouveau membre", "enssop" ),
       "view_item"             => __( "Voir le membre", "enssop" ),
       "view_items"            => __( "Voir les membres", "enssop" ),
       "search_items"          => __( "Chercher un membre", "enssop" ),
       "not_found"             => __( "Pas de membre trouvé", "enssop" ),
       "not_found_in_trash"    => __( "Pas de membre trouvé dans la corbeille", "enssop" ),
       "featured_image"        => __( "Image mise en avant pour ce membre", "enssop" ),
       "set_featured_image"    => __( "Définir l'image mise en avant pour ce membre", "enssop" ),
       "remove_featured_image" => __( "Supprimer l'image mise en avant pour ce membre", "enssop" ),
       "use_featured_image"    => __( "Utiliser comme image mise en avant pour ce membre", "enssop" ),
       "archives"              => __( "Type de membre", "enssop" ),
       "insert_into_item"      => __( "Ajouter au membre", "enssop" ),
       "uploaded_to_this_item" => __( "Ajouter au membre", "enssop" ),
       "filter_items_list"     => __( "Filtrer la liste de membres", "enssop" ),
       "items_list_navigation" => __( "Naviguer dans la liste de membres", "enssop" ),
       "items_list"            => __( "Liste de membres", "enssop" ),
       "attributes"            => __( "Paramètres de membre", "enssop" ),
       "name_admin_bar"        => __( "membre", "enssop" ),
       );

       $args= array(
           "label"     => __('membre', 'enssop'),
           "labels"    => $labels,
           "description"   =>  __('membre', 'enssop'),
           "public"    => true,
           "publicly_queryable"    => true,
           "show_ui"   =>  true,
           "delete_with_user"  =>  false,
           "show_in_rest"  => false,
           "has_archive"   =>  true,
           "show_in_menu"  =>  true,
           "show_in_nav_menu"  => true,
           "menu_position"     =>  4,
           "exclude_from_search"   => false,
           "capability_type"       => "post",
           "map_meta_cap"          => true,
           "hierarchical"          => false,
           "has_archive"           => true,
           "rewrite"               => array( "slug" => "membre", "with_front" => true ),
           "query_var"             => 'membre',
           "menu_icon"             => "dashicons-megaphone",
           "supports"              => array( "title" , 'editor' ,'author', 'thumbnail', 'excerpt', 'comments'),
       );

        register_post_type( 'membre', $args );

        /**
        * Custom Post Type : image
        */

       $labels = array(
        "name"                  => __( "Image", "enssop" ),
        "singular_name"         => __( "Image", "enssop" ),
        "menu_name"             => __( "Image", "enssop" ),
        "all_items"             => __( "Toutes les images", "enssop" ),
        "add_new"               => __( "Ajouter une image", "enssop" ),
        "add_new_item"          => __( "Ajouter une nouvelle image", "enssop" ),
        "edit_item"             => __( "Ajouter l'image", "enssop" ),
        "new_item"              => __( "Nouvelle image", "enssop" ),
        "view_item"             => __( "Voir l'image", "enssop" ),
        "view_items"            => __( "Voir les images", "enssop" ),
        "search_items"          => __( "Chercher une image", "enssop" ),
        "not_found"             => __( "Pas d'image trouvé", "enssop" ),
        "not_found_in_trash"    => __( "Pas d'image trouvé dans la corbeille", "enssop" ),
        "featured_image"        => __( "Image mise en avant pour cette image", "enssop" ),
        "set_featured_image"    => __( "Définir l'image mise en avant pour cette image", "enssop" ),
        "remove_featured_image" => __( "Supprimer l'image mise en avant pour cette image", "enssop" ),
        "use_featured_image"    => __( "Utiliser comme image mise en avant pour cette image", "enssop" ),
        "archives"              => __( "Type d'image", "enssop" ),
        "insert_into_item"      => __( "Ajouter à image", "enssop" ),
        "uploaded_to_this_item" => __( "Ajouter à image", "enssop" ),
        "filter_items_list"     => __( "Filtrer la liste des images", "enssop" ),
        "items_list_navigation" => __( "Naviguer dans la liste des images", "enssop" ),
        "items_list"            => __( "Liste des images", "enssop" ),
        "attributes"            => __( "Paramètres de image", "enssop" ),
        "name_admin_bar"        => __( "image", "enssop" ),
        );
 
        $args= array(
            "label"     => __('image', 'enssop'),
            "labels"    => $labels,
            "description"   =>  __('image', 'enssop'),
            "public"    => true,
            "publicly_queryable"    => true,
            "show_ui"   =>  true,
            "delete_with_user"  =>  false,
            "show_in_rest"  => false,
            "has_archive"   =>  true,
            "show_in_menu"  =>  true,
            "show_in_nav_menu"  => true,
            "menu_position"     =>  4,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "has_archive"           => true,
            "rewrite"               => array( "slug" => "image", "with_front" => true ),
            "query_var"             => 'image',
            "menu_icon"             => "dashicons-money",
            "supports"              => array( "title" , 'editor' ,'author', 'thumbnail', 'excerpt', 'comments'),
        );
 
         register_post_type( 'picture', $args );
 

         }

    }
  
add_action( 'init', 'deep_purple_post_type');


/**
* ajout de la feuille css
*/

if(!function_exists('addStyleSheets')){
    function addStyleSheets(){
        wp_enqueue_style( "my-stylesheet", get_stylesheet_directory_uri()."/style.css");
        wp_enqueue_style( "bootstrap-css", get_stylesheet_directory_uri()."/css/bootstrap.min.css");
        wp_enqueue_style( "font-awesome", "https://use.fontawesome.com/releases/v5.6.3/css/all.css");
        wp_enqueue_script( "jquery", "https://code.jquery.com/jquery-3.2.1.slim.min.js");
        wp_enqueue_script( "cloudflare", "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js");
        wp_enqueue_script( "bootstrap-js", get_stylesheet_directory_uri()."/js/bootstrap.min.js");
    }
}

add_action( 'wp_enqueue_scripts', 'addStyleSheets');

register_nav_menus([
    "nav_menu" =>__("Menu principal", "enssop"),
    "footer_menu" =>__("Menu du pied de page", "enssop")
] );